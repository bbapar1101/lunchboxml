﻿using Accord.MachineLearning.Bayes;
using Accord.Statistics.Distributions;
using Accord.Statistics.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProvingGround.MachineLearning.Classes
{
    [Serializable]
    public class clsNaiveBayesWrapper
    {
        public NaiveBayes<IUnivariateFittableDistribution> naiveBayes { get; set; } = null;
        public Codification codebook { get; set; } = new Codification();

        public string outputLabel = "";
    }
}
