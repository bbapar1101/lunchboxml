﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using GH_IO.Serialization;

namespace ProvingGround.MachineLearning
{
    public class MenuHeaderItem : ToolStripMenuItem
    {
        public override bool CanSelect => false;
    }
    public class CodifyDataType_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("e3340350-eaa0-4d4a-97bf-f4a83c67a37f"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            nodeCodifyData parent = this.Attributes.Parent.DocObject as nodeCodifyData;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Data Type";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 0; i < (int)CodificationVariableType.Count; i++)
            {
                Menu_AppendItem(menu, ((CodificationVariableType)i).ToString(), MenuItem_Clicked, parent.ShowMenu, parent.ShowMenu && (CodificationDataType == ((CodificationVariableType)i))).Tag = (CodificationVariableType)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                CodificationDataType = (CodificationVariableType)ts.Tag;
                nodeCodifyData parent = this.Attributes.Parent.DocObject as nodeCodifyData;
                parent.CodifyDataTypeInt = (int)CodificationDataType;

                this.PersistentData[0][0].Value = (int)CodificationDataType;

                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_CodifyDataType", (int)m_dataType);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int dtype = 0;
            if (reader.TryGetInt32("m_CodifyDataType", ref dtype))
            {
                CodificationDataType = (CodificationVariableType)dtype;
            }
            return base.Read(reader);
        }

        public CodificationVariableType CodificationDataType
        {
            get { return m_dataType; }
            set { m_dataType = value; }
        }

        private CodificationVariableType m_dataType = CodificationVariableType.Categorical;

    }
    /// <summary>
    /// Codify Data Component
    /// </summary>
    public class nodeCodifyData : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeCodifyData()
            : base("Codify Data", "Codify Data", "Codify data to be used in various classifier models.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("c35c406f-8f84-4e1c-8993-8fe6b7538c05"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_CodifyData; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("Label", "Label", "Label used to categorize this data.", GH_ParamAccess.item);
            pManager.AddTextParameter("Data", "Data", "The list of data.", GH_ParamAccess.list);
            CodifyDataType_Param cParam = new CodifyDataType_Param();
            cParam.CodificationDataType = CodificationVariableType.Categorical;
            CodifyDataTypeInt = (int)cParam.CodificationDataType;
            cParam.SetPersistentData(CodifyDataTypeInt);
            pManager.AddParameter(cParam, "Data Type", "Type", "Data Type: \n (0) Ordinal\n (1) Categorical\n (2) Categorical With Baseline\n (3) Continuous\n (4) Discrete", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Codified Data", "Data", "The codified data", GH_ParamAccess.item);
        }
        #endregion

        public int CodifyDataTypeInt = 1;
        public bool ShowMenu = true;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string label = "";
            List<string> data = new List<string>();
            clsCodifyDataItem dataItem = new Classes.clsCodifyDataItem();
            
            if (
                    DA.GetData<string>(0, ref label) &&
                    DA.GetDataList<string>(1, data)
               )
            {
                CodifyDataType_Param dataTypeParam = this.Params.Input[2] as CodifyDataType_Param;
                CodificationVariableType dType = dataTypeParam.CodificationDataType;
                CodifyDataTypeInt = (int)dType;

                if (dataTypeParam.SourceCount > 0)
                {
                    DA.GetData<int>(2, ref CodifyDataTypeInt);
                    if (CodifyDataTypeInt >= (int)CodificationVariableType.Count) CodifyDataTypeInt = (int)CodificationVariableType.Count - 1;
                    else if (CodifyDataTypeInt <= 0) CodifyDataTypeInt = 0;

                    dType = (CodificationVariableType)CodifyDataTypeInt;
                    ShowMenu = false;
                }
                else
                {
                    dataTypeParam.PersistentData[0][0].Value = CodifyDataTypeInt;
                    ShowMenu = true;
                }

                dataItem.Label = label;
                dataItem.Data = data;
                dataItem.VariableType = (CodificationVariableType)CodifyDataTypeInt;
                this.Message = ((CodificationVariableType)CodifyDataTypeInt).ToString();

                DA.SetData(0, dataItem);

            }

        }
        #endregion
    }
}



