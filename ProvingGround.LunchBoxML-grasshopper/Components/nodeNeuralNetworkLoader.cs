﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.Neuro.Networks;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using Grasshopper;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Neural Network Node
    /// </summary>
    public class nodeNeuralNetworkLoader : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeNeuralNetworkLoader()
            : base("Neural Network Loader", "Load", "Load a neural network from a file.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.hidden; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("ebaafcaa-8b8d-4991-b7d0-f2e1545501a5"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_NeuralNetwork_Loader; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("File Path", "File Path", "File path to saved neural network file (*.bin).", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Nueral Network", "Neural Network", "Loaded Neural Network", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            string fullpath = "";
            GH_String path = new GH_String();

            if (
                    DA.GetData<GH_String>(0, ref path)
               )
            {

                try
                {
                    fullpath = path.Value;
                    Accord.Neuro.ActivationNetwork network = Accord.Neuro.ActivationNetwork.Load(fullpath) as Accord.Neuro.ActivationNetwork;

                    //Output
                    DA.SetData(0, network);

                }
                catch (InvalidCastException e)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message.ToString());
                }

            }
        }
        #endregion
    }
}



