﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Accord.Statistics.Filters;
using Grasshopper;
using Accord.Statistics.Distributions;
using System.ComponentModel;
using Accord;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using GH_IO.Serialization;
using Accord.Statistics.Kernels;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.Performance;
using Accord.Math.Optimization.Losses;
using Accord.MachineLearning;
using Accord.Statistics;
using Grasshopper.Kernel.Parameters;

namespace ProvingGround.MachineLearning
{
    public enum SVMKernelType
    {
        Linear,
        Gaussian,
        Laplacian,
        Polynomial,
        Sigmoid,
        Count
    }

    public class SVMKernelType_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("b83f4fbe-99c3-4263-a84d-34124e2e83ef"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            nodeSVMKernelTrainer parent = this.Attributes.Parent.DocObject as nodeSVMKernelTrainer;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Kernel Type";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 0; i < (int)SVMKernelType.Count; i++)
            {
                Menu_AppendItem(menu, ((SVMKernelType)i).ToString(), MenuItem_Clicked, parent.ShowMenu, parent.ShowMenu && (KernelType == ((SVMKernelType)i))).Tag = (SVMKernelType)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                KernelType = (SVMKernelType)ts.Tag;
                nodeSVMKernelTrainer parent = this.Attributes.Parent.DocObject as nodeSVMKernelTrainer;
                parent.SVMKernelTypeInt = (int)KernelType;
                this.PersistentData[0][0].Value = (int)KernelType;
                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_SVMKernelType", (int)KernelType);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int dtype = 0;
            if (reader.TryGetInt32("m_SVMKernelType", ref dtype))
            {
                KernelType = (SVMKernelType)dtype;
            }
            return base.Read(reader);
        }

        public SVMKernelType KernelType
        {
            get { return m_kernelType; }
            set { m_kernelType = value; }
        }

        private SVMKernelType m_kernelType = SVMKernelType.Linear;

    }

    /// <summary>
    /// SVM Trainer Node
    /// </summary>
    public class nodeSVMKernelTrainer : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeSVMKernelTrainer()
            : base("Kernel SVM Trainer", "SVM Trainer", "This component trains a kernel-based Support Vector Machine (SVM) on an input training data set.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// 
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("6ad70fd1-5b3d-4d50-b4a5-37b9bcd7e24f"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SVM_Trainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Inputs", "Inputs", "The training input data.", GH_ParamAccess.list);
            pManager.AddGenericParameter("Training Classifications", "Classifications", "The list of classifications.", GH_ParamAccess.item);

            SVMKernelType_Param cParam = new SVMKernelType_Param();
            cParam.KernelType = SVMKernelType.Gaussian;
            SVMKernelTypeInt = (int)cParam.KernelType;
            cParam.SetPersistentData(SVMKernelTypeInt);
            pManager.AddParameter(cParam, "Kernel Type", "Kernel", "Kernel Type: \n (0) Linear \n (1) Gaussian\n (2) Laplacian\n (3) Polynomial\n (4) Sigmoid\n", GH_ParamAccess.item);
            pManager.AddIntegerParameter("K Folds", "K Folds", "The number of folds used during cross validation", GH_ParamAccess.item, 10);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Support Vector Machine", "SVM", "Trained kernel-based Support Vector Machine (SVM)", GH_ParamAccess.item);
            pManager.AddNumberParameter("Support Vectors", "Vectors", "Support Vectors", GH_ParamAccess.tree);
            pManager.AddGenericParameter("Confusion Matrix", "Confusion Matrix", "The General Confusion Matrix based on the cross-validation training/testing splits.", GH_ParamAccess.item);
        }
        #endregion

        private void SaveSVM_Clicked(object sender, EventArgs e)
        {
            if (SVMWrapper != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Support Vector Machine";
                dialog.Filter = "Generic Binary File (*.bin)|*.bin";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    Serializer.Save(SVMWrapper, fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid SVM found. Please create a valid SVM before trying to save.");
            }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Support Vector Machine", SaveSVM_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        protected override void BeforeSolveInstance()
        {
            Refresh = true;
        }

        private void SolutionCallback(GH_Document doc)
        {
            UpdateParams();
            ExpireSolution(false);
        }

        private void UpdateParams()
        {
            GH_ComponentParamServer.IGH_SyncObject syncObj = Params.EmitSyncObject();

            //Remove superflous inputs
            while(Params.Input.Count > 4)
            {
                Params.UnregisterInputParameter(Params.Input[Params.Input.Count - 1], true);
            }

            if (p_inputs.Length > 0)
            {
                for (int i = 0; i < p_inputs.Length; i += 5)
                {

                    IGH_Param param;
                    if (p_inputs[i] == "int") param = new Param_Integer();
                    else if (p_inputs[i] == "double") param = new Param_Number();
                    else param = new Param_GenericObject();

                    param.MutableNickName = false;
                    param.Name = p_inputs[i + 1];
                    param.NickName = p_inputs[i + 2];
                    param.Description = p_inputs[i + 3];
                    param.Access = GH_ParamAccess.item;
                    param.Optional = true;
                    param.AddVolatileData(new GH_Path(0), 0, Convert.ToDouble(p_inputs[i + 4]));
                    Params.RegisterInputParam(param);
                }
            }
            Params.Sync(syncObj);
            this.OnAttributesChanged();
        }

        private clsSVMWrapper SVMWrapper;
        public int SVMKernelTypeInt = 0;
        public int p_ktypeInt = 1;
        public bool NeedsUpdate = false;
        public bool Refresh;
        public bool ShowMenu = true;
        private string[] p_inputs = new string[] { };

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            if (!Refresh)
            {
                DA.DisableGapLogic();
                return;
            }
            Refresh = false;

            List<GH_ObjectWrapper> inputs = new List<GH_ObjectWrapper>();
            GH_ObjectWrapper output = new GH_ObjectWrapper();
            List<string> filters = new List<string>();
            double sigma = 0.0;
            Grasshopper.DataTree<double> vectors = new Grasshopper.DataTree<double>();
            Accord.Math.Random.Generator.Seed = 0;
            IKernel kernel;

            if (SVMWrapper == null) SVMWrapper = new clsSVMWrapper();

            SVMKernelType_Param kernelTypeParam = this.Params.Input[2] as SVMKernelType_Param;
            SVMKernelType kType = kernelTypeParam.KernelType;
            SVMKernelTypeInt = (int)kType;

            if (kernelTypeParam.SourceCount > 0)
            {
                DA.GetData<int>(2, ref SVMKernelTypeInt);
                if (SVMKernelTypeInt >= (int)SVMKernelType.Count) SVMKernelTypeInt = (int)SVMKernelType.Count - 1;
                else if (SVMKernelTypeInt <= 0) SVMKernelTypeInt = 0;

                kType = (SVMKernelType)SVMKernelTypeInt;
                ShowMenu = false;
            }
            else
            {
                kernelTypeParam.PersistentData[0][0].Value = SVMKernelTypeInt;
                ShowMenu = true;
            }

            if (kType == SVMKernelType.Polynomial)
            {
                p_inputs = new string[10] { 
                    "int", "Degree of Polynomial", "Degree", "The Degree of the Polynomial Function", "1.0", 
                    "double","Polynomial Constant", "Constant", "Polynomial Constant", "1.0" };
            }
            else if (kType == SVMKernelType.Sigmoid)
            {
                p_inputs = new string[10] {
                    "double", "Alpha Parameter", "Alpha", "Alpha parameter. Should be set to something small. Default is 0.01", "0.01",
                    "double","Polynomial Constant", "Constant", "Polynomial Constant", "1.0" };
            }
            else
            {
                p_inputs = new string[] { };
            }

            if (SVMKernelTypeInt != p_ktypeInt)
            {
                NeedsUpdate = true;
                p_ktypeInt = SVMKernelTypeInt;
            }
            else
            {
                NeedsUpdate = false;
            }

            if (NeedsUpdate)
            {
                OnPingDocument().ScheduleSolution(1, SolutionCallback);
                return;
            }


            if (
                    DA.GetDataList<GH_ObjectWrapper>(0, inputs) &&
                    DA.GetData<GH_ObjectWrapper>(1, ref output)
                )
            {
                
                DataTable table = new DataTable("Data");
                string[] columns = new string[inputs.Count + 1];

                populateColumns(ref table, ref columns, ref filters, inputs, output);
                populateRows(ref table, inputs, output);

                // create codebook
                var codebook = new Codification(table, filters.ToArray());

                DataTable symbols = codebook.Apply(table);
                string[] inputColumnNames = columns.Take(columns.Length - 1).ToArray();
                double[][] svm_inputs = symbols.ToJagged<double>(inputColumnNames);
                int[] svm_output = symbols.ToArray<int>(columns[inputs.Count]);
                int degree = 1;
                double constant = 1.0;
                double alpha = 0.01;
                int kFolds = 10;

                try
                {
                    DA.GetData<int>(3, ref kFolds);
                }
                catch (Exception e) { };

                SVMWrapper.codebook = codebook;

                switch (kType)
                {
                    case SVMKernelType.Gaussian:
                        sigma = EstimateGaussianParameters(svm_inputs);
                        break;
                    case SVMKernelType.Laplacian:
                        sigma = EstimateLaplacianParameters(svm_inputs);
                        break;
                    case SVMKernelType.Polynomial:
                        try
                        {
                            DA.GetData<int>(4, ref degree);
                            DA.GetData<double>(5, ref constant);
                        }
                        catch(Exception e){ }
                        break;
                    case SVMKernelType.Sigmoid:
                        try
                        {
                            DA.GetData<double>(4, ref alpha);
                            DA.GetData<double>(5, ref constant);
                        }
                        catch (Exception e) { }
                        break;
                }

                kernel = createKernel(sigma, degree, constant, alpha);

                var teacher = new SequentialMinimalOptimization<IKernel>()
                {
                    Tolerance = 0.001,
                    PositiveWeight = 1.0,
                    NegativeWeight = 1.0,
                    UseComplexityHeuristic = true,
                    UseKernelEstimation = true,
                    Kernel = kernel
                };

                var crossvalidation = new CrossValidation<SupportVectorMachine<IKernel, double[]>, double[]>()
                {
                    K = kFolds, 
                    Learner = (s) => new SequentialMinimalOptimization<IKernel, double[]>()
                    {
                        Tolerance = 0.001,
                        PositiveWeight = 1.0,
                        NegativeWeight = 1.0,
                        UseComplexityHeuristic = true,
                        UseKernelEstimation = true,
                        Kernel = kernel
                    },
                    Loss = (expected, actual, p) => new ZeroOneLoss(expected).Loss(actual)
                };
                crossvalidation.ParallelOptions.MaxDegreeOfParallelism = 1;

                try
                {
                    // Run Cross Validation
                    var crossValidationStatistics = crossvalidation.Learn(svm_inputs, svm_output);
                    GeneralConfusionMatrix gcm = crossValidationStatistics.ToConfusionMatrix(svm_inputs, svm_output);
                    DA.SetData(2, gcm);

                    SVMWrapper.svm = teacher.Learn(svm_inputs, svm_output);
                    double[][] v = SVMWrapper.svm.SupportVectors;

                    int path = 0;
                    foreach (double[] arr in v)
                    {
                        GH_Path p = new GH_Path(path);
                        for (int i = 0; i < arr.Length; i++)
                        {
                            double val = arr[i];
                            vectors.Insert(val, p, i);
                        }
                        path++;
                    }

                }
                catch (ConvergenceException)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Convergence could not be attained. " +
                        "The learned machine might still be usable.");
                }

                DA.SetData(0, SVMWrapper);
                DA.SetDataTree(1, vectors);

                p_ktypeInt = SVMKernelTypeInt;
            }
        }

        void populateColumns(ref DataTable table, ref string[] columns, ref List<string> filters, List<GH_ObjectWrapper> inputs, GH_ObjectWrapper output)
        {
            double d = 0.0;

            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                CodificationVariableType type = item.VariableType;

                if (Double.TryParse(item.Data[0], out d))
                {
                    table.Columns.Add(item.Label, typeof(double));
                }
                else
                {
                    table.Columns.Add(item.Label, typeof(string));
                }

                columns[i] = item.Label;

                if (type == CodificationVariableType.Ordinal ||
                    type == CodificationVariableType.CategoricalBaseline ||
                    type == CodificationVariableType.Categorical)
                {
                    filters.Add(item.Label);
                }
            }

            // add the output data to the codebook information
            clsCodifyDataItem outputItem = output.Value as clsCodifyDataItem;
            CodificationVariableType outputType = outputItem.VariableType;
            if (Double.TryParse(outputItem.Data[0], out d))
            {
                table.Columns.Add(outputItem.Label, typeof(double));
            }
            else
            {
                table.Columns.Add(outputItem.Label, typeof(string));
            }

            columns[inputs.Count] = outputItem.Label;
            SVMWrapper.outputLabel = outputItem.Label;
            if (outputType == CodificationVariableType.Ordinal ||
                outputType == CodificationVariableType.CategoricalBaseline ||
                outputType == CodificationVariableType.Categorical)
            {
                filters.Add(outputItem.Label);
            }
        }

        void populateRows(ref DataTable table, List<GH_ObjectWrapper> inputs, GH_ObjectWrapper output)
        {
            // populate the rows
            Grasshopper.DataTree<object> tree = new Grasshopper.DataTree<object>();
            clsCodifyDataItem outputItem = output.Value as clsCodifyDataItem;
            CodificationVariableType outputType = outputItem.VariableType;

            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                for (int j = 0; j < item.Data.Count; j++)
                {
                    GH_Path p = new GH_Path(j);
                    tree.Insert(item.Data[j], p, i);
                }
            }

            for (int j = 0; j < outputItem.Data.Count; j++)
            {
                GH_Path p = new GH_Path(j);
                tree.Insert(outputItem.Data[j], p, inputs.Count);
            }

            for (int i = 0; i < tree.BranchCount; i++)
            {
                table.Rows.Add(tree.Branches[i].ToArray());
            }
        }

        double EstimateGaussianParameters(double[][] inputs)
        {
            DoubleRange range; 
            Gaussian gaussian = Gaussian.Estimate(inputs, inputs.Length, out range);

            return (double)gaussian.Sigma;
        }

        double EstimateLaplacianParameters(double[][] inputs)
        {
            DoubleRange range;
            Laplacian laplacian = Laplacian.Estimate(inputs, inputs.Length, out range);

            return (double)laplacian.Sigma;
        }

        IKernel createKernel(double sigma, int degree, double constant, double alpha)
        {
            if (SVMKernelTypeInt == 0)
                return new Linear();

            else if (SVMKernelTypeInt == 1)
                return new Gaussian(sigma);

            else if (SVMKernelTypeInt == 2)
                return new Laplacian(sigma);

            else if (SVMKernelTypeInt == 3)
            {
                if (degree == 1.0)
                {
                    return new Linear(constant);
                }
                else
                {
                    return new Polynomial(degree, constant);
                }
            }
            else if (SVMKernelTypeInt == 4)
                return new Sigmoid(alpha, constant);

            else throw new Exception();
        }

        #endregion
    }
}



