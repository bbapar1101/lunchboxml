﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;


using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Accord;
using System.Windows.Navigation;
using GH_IO.Serialization;
using System.Runtime.CompilerServices;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    ///  Load A Saved Model
    /// </summary>
    public class nodeDeserializeModel : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeDeserializeModel()
            : base("Load Model", "Load Model", "Deserialize a model from a saved file (*.bin)", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quinary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("a5446931-9ca9-411f-a41c-fa5e179b26ae"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_Model_Loader; }
        }
        #endregion

        #region Inputs/Outputs

        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddTextParameter("File Path", "File Path", "File path to saved model (*.bin).", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Loaded Model", "Model", "Loaded Model", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_String path = new GH_String();

            if (
                    DA.GetData<GH_String>(0, ref path)
               )
            {
                try
                {
                    string fullpath = path.Value;
                    try
                    {
                        clsNaiveBayesWrapper naiveBayesWrapper = Serializer.Load<clsNaiveBayesWrapper>(fullpath);
                        this.Message = "Naive Bayes";
                        DA.SetData(0, naiveBayesWrapper);
                        return;
                    }
                    catch (InvalidCastException e) { }
                    try
                    {
                        var nn = Serializer.Load<Accord.Neuro.ActivationNetwork>(fullpath);
                        this.Message = "Neural Network";
                        DA.SetData(0, nn);
                        return;
                    }
                    catch (InvalidCastException e) { }
                    try
                    {
                        clsSVMWrapper svm = Serializer.Load<clsSVMWrapper>(fullpath);
                        this.Message = "Support Vector Machine";
                        DA.SetData(0, svm);
                        return;
                    }
                    catch (InvalidCastException e) { }
                }
                catch (InvalidCastException e)
                {
                    this.Message = "Incorrect Type";
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, "Incorrect model type. Please try changing the model type input to match the model being deserialized.");
                }

            }

        }
        #endregion
    }
}



