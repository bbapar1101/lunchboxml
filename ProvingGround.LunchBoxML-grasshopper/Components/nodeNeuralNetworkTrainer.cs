﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.Neuro.Networks;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Grasshopper;
using Accord.Neuro;
using GH_IO.Serialization;
using Grasshopper.Kernel.Parameters;

namespace ProvingGround.MachineLearning
{
    public enum ActivationFunction
    {
        Sigmoid,
        BipolarSigmoid,
        Linear,
        RectifiedLinear,
        Threshold,
        Identity,
        Count
    }

    public enum LearningAlgorithm
    {
        Backpropagation,
        ResilientBackpropagation,
        Evolutionary,
        LevenbergMarquardt,
        Count
    }

    public class ActivationFunction_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("831aceec-b30d-439e-99ce-bae5daf25bfe"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            nodeNeuralNetworkTrainer parent = this.Attributes.Parent.DocObject as nodeNeuralNetworkTrainer;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Activation Function";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowActivationFunctionMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 0; i < (int)ActivationFunction.Count; i++)
            {
                Menu_AppendItem(menu, ((ActivationFunction)i).ToString(), MenuItem_Clicked, parent.ShowActivationFunctionMenu, parent.ShowActivationFunctionMenu && (ActivationFunction == ((ActivationFunction)i))).Tag = (ActivationFunction)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                ActivationFunction = (ActivationFunction)ts.Tag;
                nodeNeuralNetworkTrainer parent = this.Attributes.Parent.DocObject as nodeNeuralNetworkTrainer;
                parent.ActivationFunctionInt = (int)ActivationFunction;
                this.PersistentData[0][0].Value = (int)ActivationFunction;
                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_activationFunction", (int)ActivationFunction);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int ftype = 0;
            if (reader.TryGetInt32("m_activationFunction", ref ftype))
            {
                ActivationFunction = (ActivationFunction)ftype;
            }
            return base.Read(reader);
        }

        public ActivationFunction ActivationFunction
        {
            get { return m_activationFunction; }
            set { m_activationFunction = value; }
        }

        private ActivationFunction m_activationFunction = ActivationFunction.Sigmoid;

    }

    public class LearningAlgorithm_Param : Grasshopper.Kernel.Parameters.Param_Integer
    {
        public override GH_Exposure Exposure => GH_Exposure.hidden;

        public override Guid ComponentGuid
        {
            get { return new Guid("34a22050-8484-44cf-b2ee-974739434b18"); }
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {

            nodeNeuralNetworkTrainer parent = this.Attributes.Parent.DocObject as nodeNeuralNetworkTrainer;

            Menu_AppendSeparator(menu);
            MenuHeaderItem mhi = new MenuHeaderItem();
            mhi.Text = "Learning Algorithm";
            mhi.Font = GH_FontServer.StandardItalic;
            if (!parent.ShowLearningAlgorithmMenu) mhi.Enabled = false;
            else mhi.Enabled = true;
            menu.Items.Add(mhi);

            for (int i = 0; i < (int)LearningAlgorithm.Count; i++)
            {
                Menu_AppendItem(menu, ((LearningAlgorithm)i).ToString(), MenuItem_Clicked, parent.ShowLearningAlgorithmMenu, parent.ShowLearningAlgorithmMenu && (LearningAlgorithm == ((LearningAlgorithm)i))).Tag = (LearningAlgorithm)i;
            }
        }

        private void MenuItem_Clicked(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem ts = sender as ToolStripMenuItem;
                LearningAlgorithm = (LearningAlgorithm)ts.Tag;
                nodeNeuralNetworkTrainer parent = this.Attributes.Parent.DocObject as nodeNeuralNetworkTrainer;
                parent.ActivationFunctionInt = (int)LearningAlgorithm;
                this.PersistentData[0][0].Value = (int)LearningAlgorithm;
                ExpireSolution(true);
            }
        }

        public override bool Write(GH_IWriter writer)
        {
            writer.SetInt32("m_learningAlgorithm", (int)LearningAlgorithm);
            return base.Write(writer);
        }
        public override bool Read(GH_IReader reader)
        {
            int ltype = 0;
            if (reader.TryGetInt32("m_learningAlgorithm", ref ltype))
            {
                LearningAlgorithm = (LearningAlgorithm)ltype;
            }
            return base.Read(reader);
        }

        public LearningAlgorithm LearningAlgorithm
        {
            get { return m_learningAlgorithm; }
            set { m_learningAlgorithm = value; }
        }

        private LearningAlgorithm m_learningAlgorithm = LearningAlgorithm.ResilientBackpropagation;

    }

    /// <summary>
    /// Neural Network Node
    /// </summary>
    public class nodeNeuralNetworkTrainer : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeNeuralNetworkTrainer()
            : base("Neural Network Trainer", "Neural Network Trainer", "This component uses the resilient backpropagation (RProp) learning algorithm to train neural networks.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("cc010201-a5bf-439e-8190-cadb8019b820"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_NeuralNetwork_Trainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddNumberParameter("Training Inputs", "Inputs", "The training inputs values", GH_ParamAccess.tree);
            pManager.AddNumberParameter("Training Outputs", "Outputs", "The expected outputs values", GH_ParamAccess.tree);
            pManager.AddIntegerParameter("Hidden Neurons", "Hidden Neurons", "Number of neurons in each hidden layer.", GH_ParamAccess.list, 6);

            LearningAlgorithm_Param LAparam = new LearningAlgorithm_Param();
            LAparam.LearningAlgorithm = LearningAlgorithm.ResilientBackpropagation;
            LearningAlgorithmInt = (int)LAparam.LearningAlgorithm;
            LAparam.SetPersistentData(LearningAlgorithmInt);
            pManager.AddParameter(LAparam, "Learning Algorithm", "Learning Algorithm", "Learning Algorithm: \n (0) Backpropagation\n (1) Resilient Backpropagation\n (2) Evolutionary (Genetic) Algorithm\n (3) Levenberg-Marquardt", GH_ParamAccess.item);

            ActivationFunction_Param AFparam = new ActivationFunction_Param();
            AFparam.ActivationFunction = ActivationFunction.Sigmoid;
            ActivationFunctionInt = (int)AFparam.ActivationFunction;
            AFparam.SetPersistentData(ActivationFunctionInt);
            pManager.AddParameter(AFparam, "Activation Function", "Activation Function", "Activation Function: \n (0) Sigmoid\n (1) Bipolar Sigmoid\n (2) Linear\n (3) Rectified Linear\n (4) Threshold\n (5) Identity", GH_ParamAccess.item);
            
            pManager.AddNumberParameter("Alpha", "Alpha", "Alpha value.", GH_ParamAccess.item, 2.0);
            pManager.AddIntegerParameter("Iterations", "Iterations", "Number of iterations to teach the network.", GH_ParamAccess.item, 500);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trained Nueral Network", "Neural Network", "Trained Neural Network", GH_ParamAccess.item);
            pManager.AddNumberParameter("Error's Dynamics ", "Error", "Error's Dynamics", GH_ParamAccess.list);
        }
        #endregion

        private void SaveNeuralNetwork_Clicked(object sender, EventArgs e)
        {
            if(network != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Neural Network";
                dialog.Filter = "Generic Binary File (*.bin)|*.bin";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    network.Save(fullpath);
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid neural network found. Please create a valid neural network before trying to save.");
            }        
        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Neural Network", SaveNeuralNetwork_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        protected override void BeforeSolveInstance()
        {
            Refresh = true;
        }

        private void SolutionCallback(GH_Document doc)
        {
            UpdateParams();
            ExpireSolution(false);
        }

        private void UpdateParams()
        {
            GH_ComponentParamServer.IGH_SyncObject syncObj = Params.EmitSyncObject();

            //Remove superflous inputs
            while (Params.Input.Count > 7)
            {
                Params.UnregisterInputParameter(Params.Input[Params.Input.Count - 1], true);
            }

            if (p_inputs.Length > 0)
            {
                for (int i = 0; i < p_inputs.Length; i += 5)
                {

                    IGH_Param param;
                    if (p_inputs[i] == "int") param = new Param_Integer();
                    else if (p_inputs[i] == "double") param = new Param_Number();
                    else if (p_inputs[i] == "boolean") param = new Param_Boolean();
                    else param = new Param_GenericObject();

                    param.MutableNickName = false;
                    param.Name = p_inputs[i + 1];
                    param.NickName = p_inputs[i + 2];
                    param.Description = p_inputs[i + 3];
                    param.Access = GH_ParamAccess.item;
                    param.Optional = true;
                    if (p_inputs[i] == "boolean")
                    {
                        param.AddVolatileData(new GH_Path(0), 0, Convert.ToBoolean(p_inputs[i + 4]));
                    }
                    else
                    {
                        param.AddVolatileData(new GH_Path(0), 0, Convert.ToDouble(p_inputs[i + 4]));
                    }
                        
                    Params.RegisterInputParam(param);
                }
            }
            Params.Sync(syncObj);
            this.OnAttributesChanged();
        }

        Accord.Neuro.ActivationNetwork network = null;
        public int ActivationFunctionInt = 0;
        public int LearningAlgorithmInt = 1;
        public bool ShowActivationFunctionMenu = true;
        public bool ShowLearningAlgorithmMenu = true;
        public int p_LearnerInt = 0;
        public bool NeedsUpdate = false;
        public bool Refresh;
        private string[] p_inputs = new string[] { };

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            if (!Refresh)
            {
                DA.DisableGapLogic();
                return;
            }
            Refresh = false;

            // Tree Structure Input Variables         
            GH_Structure<GH_Number> inputs = new GH_Structure<GH_Number>();
            GH_Structure<GH_Number> outputs = new GH_Structure<GH_Number>();

            List<double> errorRate = new List<double>();

            List<int> numOfHiddenNeurons = new List<int>();
            int numOfIterations = 100;
            double alpha = 2.0;
            DataTree<double> weights = new DataTree<double>();
            DataTree<double> bias = new DataTree<double>();
            double learningRate = 0.1;
            double momentum = 0.0;
            int chromosomes = 100;
            bool useRegularization = false;
            double adjustment = 10;
            double bayesianAlpha = 0.1;
            double bayesianBeta = 1.0;


            //Variables
            if  (
                    DA.GetDataTree<GH_Number>(0, out inputs) &&
                    DA.GetDataTree<GH_Number>(1, out outputs) &&
                    DA.GetDataList(2, numOfHiddenNeurons) &&

                    DA.GetData(5, ref alpha) &&
                    DA.GetData(6, ref numOfIterations)
                )
            {
                // Learning Algorithm Parameter
                LearningAlgorithm_Param LearningAlgorithmParam = this.Params.Input[3] as LearningAlgorithm_Param;
                LearningAlgorithm learningType = LearningAlgorithmParam.LearningAlgorithm;
                LearningAlgorithmInt = (int)learningType;

                if (LearningAlgorithmParam.SourceCount > 0)
                {
                    DA.GetData<int>(3, ref LearningAlgorithmInt);
                    if (LearningAlgorithmInt >= (int)LearningAlgorithm.Count) LearningAlgorithmInt = (int)LearningAlgorithm.Count - 1;
                    else if (LearningAlgorithmInt <= 0) LearningAlgorithmInt = 0;

                    learningType = (LearningAlgorithm)LearningAlgorithmInt;
                    ShowLearningAlgorithmMenu = false;
                }
                else
                {
                    LearningAlgorithmParam.PersistentData[0][0].Value = LearningAlgorithmInt;
                    ShowLearningAlgorithmMenu = true;
                }

                if (learningType == LearningAlgorithm.Backpropagation)
                {
                    p_inputs = new string[10] {
                    "double", "Learning Rate", "Learning Rate", "This value determines the speed of learning.", "0.1",
                    "double","Momentum", "Momentum", "This value determines the portion of previous weight's update to use on the current iteration. Weight's update values are calculated on each iteration depending on neuron's error. The momentum specifies the amount of update to use from previous iteration and the amount of update to use from current iteration. If the value is equal to 0.1, for example, then 0.1 portion of previous update and 0.9 portion of current update are used to update weight's value.", "0.0" };
                }
                else if (learningType == LearningAlgorithm.Evolutionary)
                {
                    p_inputs = new string[5] {
                    "int", "Chromosomes", "Chromosomes", "The number of chromosomes in a genetic population.", "100"};
                }
                else if (learningType == LearningAlgorithm.LevenbergMarquardt)
                {
                    p_inputs = new string[25] {
                    "boolean", "Use Regularization", "Regularization", "Use Bayesian Regularization.", "false",
                    "double", "Learning Rate", "Learning Rate", "This value determines the speed of learning.", "0.1",
                    "double", "Adjustment", "Adjustment", "The learning rate adjustment.", "10.0",
                    "double", "Bayesian Alpha", "Bayesian Alpha", "The first Bayesian hyperparameter sets the importance of the squared sum of the network weights in the cost function. Used by the bayesian regularization algorithm.", "0.0",
                    "double", "Bayesian Beta", "Bayesian Beta", "The second Bayesian hyperparameter sets the importance of the squared sum of the network weights in the cost function. Used by the bayesian regularization algorithm.", "1.0"};
                }
                else
                {
                    p_inputs = new string[] { };
                }

                // Activation Function Parameter
                ActivationFunction_Param ActivationFunctionParam = this.Params.Input[4] as ActivationFunction_Param;
                ActivationFunction funcType = ActivationFunctionParam.ActivationFunction;
                ActivationFunctionInt = (int)funcType;

                if (ActivationFunctionParam.SourceCount > 0)
                {
                    DA.GetData<int>(4, ref ActivationFunctionInt);
                    if (ActivationFunctionInt >= (int)ActivationFunction.Count) ActivationFunctionInt = (int)ActivationFunction.Count - 1;
                    else if (ActivationFunctionInt <= 0) ActivationFunctionInt = 0;

                    funcType = (ActivationFunction)ActivationFunctionInt;
                    ShowActivationFunctionMenu = false;
                }
                else
                {
                    ActivationFunctionParam.PersistentData[0][0].Value = ActivationFunctionInt;
                    ShowActivationFunctionMenu = true;
                }


                if (LearningAlgorithmInt != p_LearnerInt)
                {
                    NeedsUpdate = true;
                    p_LearnerInt = LearningAlgorithmInt;
                }
                else
                {
                    NeedsUpdate = false;
                }

                if (NeedsUpdate)
                {
                    OnPingDocument().ScheduleSolution(1, SolutionCallback);
                    return;
                }


                // list of lists
                List<List<double>> inputList = new List<List<double>>();
                List<List<double>> outputList = new List<List<double>>();

                // add input list
                for (int i = 0; i < inputs.Branches.Count; i++)
                {
                    List<double> list = new List<double>();
                    List<GH_Number> branch = inputs.Branches[i];
                    foreach (GH_Number num in branch)
                    {
                        list.Add(num.Value);
                    }
                    inputList.Add(list);
                }

                // add output list with remapped values 
                for (int i = 0; i < outputs.Branches.Count; i++)
                {
                    List<double> list = new List<double>();
                    List<GH_Number> branch = outputs.Branches[i];
                    foreach (GH_Number num in branch)
                    {
                        list.Add(num.Value);
                    }
                    outputList.Add(list);
                }

                switch (learningType)
                {
                    case LearningAlgorithm.Backpropagation:
                        try
                        {
                            DA.GetData<double>(7, ref learningRate);
                            DA.GetData<double>(8, ref momentum);
                        }
                        catch (Exception e) { }
                        break;
                    case LearningAlgorithm.Evolutionary:
                        try
                        {
                            DA.GetData<int>(7, ref chromosomes);
                        }
                        catch (Exception e) { }
                        break;
                    case LearningAlgorithm.LevenbergMarquardt:
                        try
                        {
                            DA.GetData<bool>(7, ref useRegularization);
                            DA.GetData<double>(8, ref learningRate);
                            DA.GetData<double>(9, ref adjustment);
                            DA.GetData<double>(10, ref bayesianAlpha);
                            DA.GetData<double>(11, ref bayesianBeta);
                        }
                        catch (Exception e) { }
                        break;
                }

                //Result
                clsML ML = new Classes.clsML();
                network = ML.BackPropagationTrainer(inputList, outputList, LearningAlgorithmParam.LearningAlgorithm, ActivationFunctionParam.ActivationFunction, numOfHiddenNeurons, alpha, numOfIterations, learningRate, momentum, chromosomes, useRegularization, adjustment, bayesianAlpha, bayesianBeta, ref errorRate);

                int p = 0;
                
                foreach(var layer in network.Layers)
                {
                    GH_Path path = new GH_Path(p);
                    foreach (ActivationNeuron neuron in layer.Neurons)
                    {
                        bias.Add(neuron.Threshold);
                        foreach (double weight in neuron.Weights)
                        {
                            weights.Add(weight, path);
                        }
                    }
                    p++;
                }

                //Output
                DA.SetData(0, network);
                DA.SetDataList(1, errorRate);

                p_LearnerInt = LearningAlgorithmInt;

            }
        }
        #endregion
    }
}



