﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Accord.Statistics.Filters;
using Grasshopper;
using Accord.Statistics.Distributions;
using System.ComponentModel;
using Accord;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Accord.MachineLearning;
using Accord.Math.Optimization.Losses;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Naive Bayes Trainer Node
    /// </summary>
    public class nodeNaiveBayesTrainer : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeNaiveBayesTrainer()
            : base("Naive Bayes Classification Trainer", "Naive Bayes Classification Trainer", "This component trains an adaptive naive bayes classifier algorithm based on a training data set.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("f568a52c-8793-46e9-a3cc-4d44a52f495b"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_NaiveBayes_Trainer; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Training Inputs", "Inputs", "The training input data.", GH_ParamAccess.list);
            pManager.AddGenericParameter("Training Classifications", "Classifications", "The list of classifications.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Trained Naive Bayes Classifier", "Classifier", "Trained Naive Bayes Classifier", GH_ParamAccess.item);
            pManager.AddGenericParameter("Confusion Matrix", "Confusion Matrix", "The General Confusion Matrix based on the cross-validation training/testing splits.", GH_ParamAccess.item);
        }
        #endregion

        private void SaveNaiveBayesClassifier_Clicked(object sender, EventArgs e)
        {
            if (naiveBayesWrapper != null)
            {
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
                dialog.Title = "Save Naive Bayes Classifier";
                dialog.Filter = "Generic Binary File (*.bin)|*.bin";
                dialog.FilterIndex = 1;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string fullpath = Path.GetFullPath(dialog.FileName);
                    Serializer.Save(naiveBayesWrapper, fullpath);                  
                }
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("No valid classifier found. Please create a valid naive bayes classifier before trying to save.");
            }

        }

        public override void AppendAdditionalMenuItems(ToolStripDropDown menu)
        {
            base.AppendAdditionalMenuItems(menu);

            Menu_AppendSeparator(menu);
            Menu_AppendItem(menu, "Save Naive Bayes Classifier", SaveNaiveBayesClassifier_Clicked, Properties.Resources.PG_ML_SaveFile);
        }

        private clsNaiveBayesWrapper naiveBayesWrapper = null;

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {      
            List<GH_ObjectWrapper> inputs = new List<GH_ObjectWrapper>();
            GH_ObjectWrapper output = new GH_ObjectWrapper();
            List<string> filters = new List<string>();

            if (naiveBayesWrapper == null) naiveBayesWrapper = new clsNaiveBayesWrapper();

            if (
                    DA.GetDataList<GH_ObjectWrapper>(0, inputs) &&
                    DA.GetData<GH_ObjectWrapper>(1, ref output) 
                )
            {
                DataTable table = new DataTable("Data");
                string[] columns = new string[inputs.Count + 1];

                populateColumns(ref table, ref columns, ref filters, inputs, output);
                populateRows(ref table, inputs, output);
                
                // create codebook
                var codebook = new Codification(table, filters.ToArray());

                IUnivariateFittableDistribution[] priors = new IUnivariateFittableDistribution[inputs.Count + 1];
                populatePriors(ref priors, ref codebook, inputs, output);

                DataTable symbols = codebook.Apply(table);
                string[] inputColumnNames = columns.Take(columns.Length - 1).ToArray();
                double[][] nb_inputs = symbols.ToJagged<double>(inputColumnNames);
                int[] nb_output = symbols.ToArray<int>(columns[inputs.Count]);

                var crossvalidation = CrossValidation.Create(
                    k: 10,
                    learner: (p) => new NaiveBayesLearning<IUnivariateFittableDistribution>()
                    {
                        Distribution = (classIndex, variableIndex) => priors[variableIndex]
                    },
                    loss: (actual, expected, p) => new ZeroOneLoss(expected).Loss(actual),
                    fit: (teacher, x, y, w) => teacher.Learn(x, y, w),
                    x: nb_inputs, y: nb_output
                );

                var learner = new NaiveBayesLearning<IUnivariateFittableDistribution>()
                {
                    Distribution = (classIndex, variableIndex) => priors[variableIndex]
                };

                // Run Cross Validation
                var crossValidationStatistics = crossvalidation.Learn(nb_inputs, nb_output);
                GeneralConfusionMatrix gcm = crossValidationStatistics.ToConfusionMatrix(nb_inputs, nb_output);
                DA.SetData(1, gcm);

                naiveBayesWrapper.naiveBayes = learner.Learn(nb_inputs, nb_output);
                naiveBayesWrapper.codebook = codebook;
                
                DA.SetData(0, naiveBayesWrapper);
            }
        }

        void populateColumns(ref DataTable table, ref string[] columns, ref List<string> filters, List<GH_ObjectWrapper> inputs, GH_ObjectWrapper output)
        {
            double d = 0.0;

            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                CodificationVariableType type = item.VariableType;

                if (Double.TryParse(item.Data[0], out d))
                {
                    table.Columns.Add(item.Label, typeof(double));
                }
                else
                {
                    table.Columns.Add(item.Label, typeof(string));
                }

                columns[i] = item.Label;

                if (type == CodificationVariableType.Ordinal ||
                    type == CodificationVariableType.CategoricalBaseline ||
                    type == CodificationVariableType.Categorical)
                {
                    filters.Add(item.Label);
                }
            }

            // add the output data to the codebook information
            clsCodifyDataItem outputItem = output.Value as clsCodifyDataItem;
            CodificationVariableType outputType = outputItem.VariableType;
            if (Double.TryParse(outputItem.Data[0], out d))
            {
                table.Columns.Add(outputItem.Label, typeof(double));
            }
            else
            {
                table.Columns.Add(outputItem.Label, typeof(string));
            }

            columns[inputs.Count] = outputItem.Label;
            naiveBayesWrapper.outputLabel = outputItem.Label;
            if (outputType == CodificationVariableType.Ordinal ||
                outputType == CodificationVariableType.CategoricalBaseline ||
                outputType == CodificationVariableType.Categorical)
            {
                filters.Add(outputItem.Label);
            }
        }

        void populateRows(ref DataTable table, List<GH_ObjectWrapper> inputs, GH_ObjectWrapper output)
        {
            // populate the rows
            Grasshopper.DataTree<object> tree = new Grasshopper.DataTree<object>();
            clsCodifyDataItem outputItem = output.Value as clsCodifyDataItem;
            CodificationVariableType outputType = outputItem.VariableType;

            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                for (int j = 0; j < item.Data.Count; j++)
                {
                    GH_Path p = new GH_Path(j);
                    tree.Insert(item.Data[j], p, i);
                }
            }

            for (int j = 0; j < outputItem.Data.Count; j++)
            {
                GH_Path p = new GH_Path(j);
                tree.Insert(outputItem.Data[j], p, inputs.Count);
            }

            for (int i = 0; i < tree.BranchCount; i++)
            {
                table.Rows.Add(tree.Branches[i].ToArray());
            }
        }

        void populatePriors(ref IUnivariateFittableDistribution[] priors, ref Codification codebook, List<GH_ObjectWrapper> inputs, GH_ObjectWrapper output)
        {
            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                definePrior(ref priors, i, item, codebook);
            }

            clsCodifyDataItem outputItem = output.Value as clsCodifyDataItem;
            definePrior(ref priors, inputs.Count, outputItem, codebook);
        }

        void definePrior(ref IUnivariateFittableDistribution[] priors, int index, clsCodifyDataItem item, Codification codebook)
        {
            CodificationVariableType type = item.VariableType;
            switch (type)
            {
                case CodificationVariableType.Ordinal:
                    priors[index] = new NormalDistribution();
                    break;
                case CodificationVariableType.Categorical:
                    priors[index] = new GeneralDiscreteDistribution(codebook[item.Label].NumberOfSymbols);
                    break;
                case CodificationVariableType.CategoricalBaseline:
                    priors[index] = new GeneralDiscreteDistribution(codebook[item.Label].NumberOfSymbols);
                    break;
                case CodificationVariableType.Continuous:
                    priors[index] = new NormalDistribution();
                    break;
                case CodificationVariableType.Discrete:
                    priors[index] = new GeneralDiscreteDistribution(codebook[item.Label].NumberOfSymbols);
                    break;
            }
        }
        #endregion
    }
}



