﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.Neuro.Networks;

using ProvingGround.MachineLearning.Classes;
using Grasshopper;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Neural Network Node
    /// </summary>
    public class nodeNeuralNetworkTesting : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeNeuralNetworkTesting()
            : base("Neural Network Tester", "Neural Network Tester", "Test a solution using a trained neural network solver.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.quarternary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("b2417354-3be4-441c-897d-b1f9fc0b71dd"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_NeuralNetwork_Tester; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Trained Neural Network", "Neural Network", "A trained neural network.", GH_ParamAccess.item);
            pManager.AddNumberParameter("Test Input Data", "Input", "Test input data. Assumes values have been remapped to [-1 to 1] domain.", GH_ParamAccess.tree);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("Neural Network Output", "Output", "Output from the neural network", GH_ParamAccess.tree);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            // Tree Structure Input Variables         
            GH_Structure<GH_Number> inputs = new GH_Structure<GH_Number>();
            GH_ObjectWrapper m_network = new GH_ObjectWrapper();

           //Tree Variables
            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref m_network) &&
                    DA.GetDataTree<GH_Number>(1, out inputs) 
                )
            {

                try
                {
                    Accord.Neuro.ActivationNetwork network = m_network.Value as Accord.Neuro.ActivationNetwork;
                    List<List<double>> inputList = new List<List<double>>();

                    for (int i = 0; i < inputs.Branches.Count; i++)
                    {
                        List<double> list = new List<double>();
                        List<GH_Number> branch = inputs.Branches[i];
                        foreach (GH_Number num in branch)
                        {
                            list.Add(num.Value);
                        }
                        inputList.Add(list);
                    }

                    //Result
                    clsML ML = new Classes.clsML();
                    double[][] output = ML.BackPropagationTester(network, inputList);

                    int branchCount = output.GetLength(0);

                    IList<GH_Path> inputPaths = inputs.Paths;

                    DataTree<double> NN_output = new DataTree<double>();

                    for(int i = 0; i < branchCount; i++)
                    {
                        GH_Path path = inputPaths[i];
                        foreach (double n in output[i])
                        {
                            NN_output.Add(n, path);
                        }
                    }

                    DA.SetDataTree(0, NN_output);
                }
                catch (InvalidCastException e)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message.ToString());
                }

                

            }
        }
        #endregion
    }
}



