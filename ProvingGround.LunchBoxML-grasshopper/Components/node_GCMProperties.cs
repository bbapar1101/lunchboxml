﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Rhino.PlugIns;
using Accord.Statistics.Kernels;
using Accord.Statistics.Distributions;
using Accord.Statistics.Filters;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.MachineLearning.VectorMachines;
using Accord.Statistics;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Expose properties of a General Confusion Matrix
    /// </summary>
    public class nodeGCMProperties : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeGCMProperties()
            : base("Confusion Matrix Properties", "Confusion Matrix", "This component exposes the properties of a General Confusion Matrix", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("72051c7a-6dd4-41e6-a85b-94e6d57fd250"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_ConfusionMatrix; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("General Confusion Matrix", "Confusion Matrix", "The General Confusion Matrix.", GH_ParamAccess.item);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddNumberParameter("Accuracy", "Accuracy", "Accuracy (or Overall Agreement)", GH_ParamAccess.item);
            pManager.AddNumberParameter("Error", "Error", "This is the same as 1.0 - Accuracy", GH_ParamAccess.item);
            pManager.AddNumberParameter("Precision", "Precision", "Precision", GH_ParamAccess.list);
            pManager.AddNumberParameter("Recall", "Recall", "Recall", GH_ParamAccess.list);
            pManager.AddNumberParameter("Variance", "Variance", "Variance", GH_ParamAccess.item);
            pManager.AddNumberParameter("Chance Agreement", "Chance Agreement", "Chance Agreement", GH_ParamAccess.item);
            pManager.AddNumberParameter("Geometric Agreement", "Geometric Agreement", "Geometric Agreement", GH_ParamAccess.item);
            pManager.AddNumberParameter("Cohen's Kappa Statistic", "Kappa", "Cohen's Kappa statistic", GH_ParamAccess.item);
            pManager.AddNumberParameter("Chi-Square", "Chi-Square", "The Chi-Square statistic for the contingency table. ", GH_ParamAccess.item);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            GH_ObjectWrapper cm = new GH_ObjectWrapper();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref cm)
                )
            {
                try
                {
                    GeneralConfusionMatrix gcm = cm.Value as GeneralConfusionMatrix;

                    DA.SetData(0, gcm.Accuracy);
                    DA.SetData(1, gcm.Error);
                    DA.SetDataList(2, gcm.Precision.ToList());
                    DA.SetDataList(3, gcm.Recall.ToList());
                    DA.SetData(4, gcm.Variance);
                    DA.SetData(5, gcm.ChanceAgreement);
                    DA.SetData(6, gcm.GeometricAgreement);
                    DA.SetData(7, gcm.Kappa);
                    DA.SetData(8, gcm.ChiSquare);

                }
                catch (Exception e) { };
            }

        }
        #endregion
    }
}



