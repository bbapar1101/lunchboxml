﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Accord.Diagnostics;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Data;
using Grasshopper.Kernel.Types;
using Accord.MachineLearning.Bayes;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Distributions.Univariate;
using Accord.IO;

using ProvingGround.MachineLearning.Classes;
using System.Windows.Forms;
using System.IO;
using Rhino.PlugIns;
using Accord.Statistics.Kernels;
using Accord.Statistics.Distributions;
using Accord.Statistics.Filters;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.MachineLearning.VectorMachines;
using Accord.Statistics;

namespace ProvingGround.MachineLearning
{
    /// <summary>
    /// Naive Bayes Tester Node
    /// </summary>
    public class nodeSVMKernelTester : GH_Component
    {
        #region Register Node

        /// <summary>
        /// Load Node Template
        /// </summary>
        public nodeSVMKernelTester()
            : base("Kernel SVM Tester", "SVM Tester", "This component tests an kernel-based Support Vector Machine (SVM) classifier.", "LunchBox", "Machine Learning")
        {

        }

        /// <summary>
        /// Component Exposure
        /// </summary>
        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.tertiary; }
        }

        /// <summary>
        /// GUID generator http://www.guidgenerator.com/online-guid-generator.aspx
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("6d78b308-bcfd-4821-9365-aea5cfd9178b"); }
        }

        /// <summary>
        /// Icon 24x24
        /// </summary>
        protected override Bitmap Icon
        {
            get { return Properties.Resources.PG_ML_SVM_Tester; }
        }
        #endregion

        #region Inputs/Outputs
        /// <summary>
        /// Node inputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            pManager.AddGenericParameter("Support Vector Machine", "SVM", "The Support Vector Machine to test.", GH_ParamAccess.item);
            pManager.AddGenericParameter("Test Input Data", "Input", "Test input data to classify.", GH_ParamAccess.list);
        }

        /// <summary>
        /// Node outputs
        /// </summary>
        /// <param name="pManager"></param>
        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            pManager.AddGenericParameter("Classifications", "Classifications", "Classifications based on input data.", GH_ParamAccess.list);
            pManager.AddNumberParameter("Probabilties", "Probabilties", "Probabilty that an input vector belongs to its predicted classification.", GH_ParamAccess.tree);
        }
        #endregion

        #region Solution
        /// <summary>
        /// Code by the component
        /// </summary>
        /// <param name="DA"></param>
        protected override void SolveInstance(IGH_DataAccess DA)
        {
            List<GH_ObjectWrapper> inputs = new List<GH_ObjectWrapper>();
            GH_ObjectWrapper svm_input = new GH_ObjectWrapper();
            Grasshopper.DataTree<double> probabilities = new Grasshopper.DataTree<double>();
            List<string> filters = new List<string>();
            List<string> translated = new List<string>();

            if (
                    DA.GetData<GH_ObjectWrapper>(0, ref svm_input) &&
                    DA.GetDataList<GH_ObjectWrapper>(1, inputs)
                )
            {
                try
                {
                    clsSVMWrapper SVMWrapper = svm_input.Value as clsSVMWrapper;

                    DataTable table = new DataTable("Data");
                    string[] columns = new string[inputs.Count];

                    populateColumns(ref table, ref columns, ref filters, inputs);
                    populateRows(ref table, inputs);

                    // create codebook
                    Codification codebook = SVMWrapper.codebook;

                    DataTable symbols = codebook.Apply(table);
                    double[][] svm_inputs = symbols.ToJagged<double>(columns);
                    int[] svmClassifications = SVMWrapper.svm.Decide(svm_inputs).ToZeroOne();
                    //translated = codebook.Revert(SVMWrapper.outputLabel, svmClassifications).ToList();
                    double[][] nbProbability = SVMWrapper.svm.Probabilities(svm_inputs);

                    int path = 0;
                    foreach (double[] arr in nbProbability)
                    {
                        GH_Path p = new GH_Path(path);
                        for (int i = 0; i < arr.Length; i++)
                        {
                            double val = arr[i];
                            if (Double.IsNaN(val)) val = 0.0;
                            probabilities.Insert(val, p, i);
                        }
                        path++;
                    }

                    DA.SetDataList(0, svmClassifications.ToList());
                    DA.SetDataTree(1, probabilities);
                }
                catch (InvalidCastException e)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Warning, e.Message.ToString());
                }
            }
        }

        void populateColumns(ref DataTable table, ref string[] columns, ref List<string> filters, List<GH_ObjectWrapper> inputs)
        {
            // populate the columns
            double d = 0.0;

            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                CodificationVariableType type = item.VariableType;

                if (Double.TryParse(item.Data[0], out d))
                {
                    table.Columns.Add(item.Label, typeof(double));
                }
                else
                {
                    table.Columns.Add(item.Label, typeof(string));
                }

                columns[i] = item.Label;

                if (type == CodificationVariableType.Ordinal ||
                type == CodificationVariableType.CategoricalBaseline ||
                type == CodificationVariableType.Categorical)
                {
                    filters.Add(item.Label);
                }
            }
        }

        void populateRows(ref DataTable table, List<GH_ObjectWrapper> inputs)
        {
            // populate the rows
            Grasshopper.DataTree<object> tree = new Grasshopper.DataTree<object>();

            for (int i = 0; i < inputs.Count; i++)
            {
                clsCodifyDataItem item = inputs[i].Value as clsCodifyDataItem;
                for (int j = 0; j < item.Data.Count; j++)
                {
                    GH_Path p = new GH_Path(j);
                    tree.Insert(item.Data[j], p, i);
                }
            }

            for (int i = 0; i < tree.BranchCount; i++)
            {
                table.Rows.Add(tree.Branches[i].ToArray());
            }
        }
        #endregion
    }
}



